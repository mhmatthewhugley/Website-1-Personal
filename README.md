# Website-1-Personal
This is my original/old website started on 6-5-2020. On 6-6-2020, I started working on this until the end of the year, and it was originally a dynamic website. Now it is a static, "archived" website.

The original website domain name is "matthewh.XYZ"

The current one is "https://mhmatthewhugley.gitlab.io/Website-1-Personal/". That link is through GitLab.

Copyright © 2020,2021 Matthew Hugley. All Rights Reserved.
